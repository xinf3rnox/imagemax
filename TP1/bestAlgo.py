import cv2 as cv2
from matplotlib import pyplot as plt
import sys, getopt
from seuilGris import *
from evaluation import *
from sobel import *
import matplotlib.pyplot as plt

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('bestAlgo.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('bestAlgo.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    imageTrue = cv2.imread(inputfile, 0)
    imagePerso = cv2.imread(outputfile, 0)

    p = 0
    r = 0
    best_seuil = 0
    p_list = []
    r_list = []

    # laplacian and gaussian and seuil de gris
    gaussian = cv2.GaussianBlur(imagePerso, (3, 3), 0)
    laplace = cv2.Laplacian(gaussian, -1)
    for i in range(0, 255):
        seuil_gris = seuilgris(laplace, i, cv2.THRESH_BINARY)
        p_next, r_next = evaluation(seuil_gris, imageTrue)
        p_list.append(p_next)
        r_list.append(r_next)
        if sqrt(pow(1 - p_next, 2) + pow(1 - r_next, 2)) < sqrt(pow(1 - p, 2) + pow(1 - r, 2)):
            p = p_next
            r = r_next
            best_seuil = i

    plt.plot(p_list, r_list)
    plt.savefig('laplce.png')
    print("meilleur laplacian, gaussian : seuil=" + str(best_seuil) + " p=" + str(p) + " r=" + str(r))

    p = 0
    r = 0
    best_seuil = 0
    p_list = []
    r_list = []

    gaussian = cv2.GaussianBlur(imagePerso, (3, 3), 0)
    after_sobel = sobel(gaussian)  # Seuil fixe pour l'instant
    for i in range(0, 255):
        seuil_gris = seuilgris(after_sobel, i, cv2.THRESH_BINARY)
        p_next, r_next = evaluation(seuil_gris, imageTrue)
        p_list.append(p_next)
        r_list.append(r_next)
        if sqrt(pow(1 - p_next, 2) + pow(1 - r_next, 2)) < sqrt(pow(1 - p, 2) + pow(1 - r, 2)):
            p = p_next
            r = r_next
            best_seuil = i

    plt.plot(p_list, r_list)
    plt.savefig('sobel.png')
    print("meilleur sobel, gaussian : seuil=" + str(best_seuil) + " p=" + str(p) + " r=" + str(r))

    p = 0
    r = 0
    best_seuil = 0
    p_list = []
    r_list = []

    gaussian = cv2.GaussianBlur(imagePerso, (3, 3), 0)
    equalize = cv2.equalizeHist(gaussian)
    after_sobel = sobel(equalize)  # Seuil fixe pour l'instant
    for i in range(0, 255):
        seuil_gris = seuilgris(after_sobel, i, cv2.THRESH_BINARY)
        p_next, r_next = evaluation(seuil_gris, imageTrue)
        p_list.append(p_next)
        r_list.append(r_next)
        if sqrt(pow(1 - p_next, 2) + pow(1 - r_next, 2)) < sqrt(pow(1 - p, 2) + pow(1 - r, 2)):
            p = p_next
            r = r_next
            best_seuil = i

    plt.plot(p_list, r_list)
    plt.savefig('equalize.png')
    print("meilleur equalize, sobel, gaussian : seuil=" + str(best_seuil) + " p=" + str(p) + " r=" + str(r))


if __name__ == "__main__":
    main(sys.argv[1:])
