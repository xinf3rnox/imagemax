import math

import cv2 as cv2
from matplotlib import pyplot as plt
from numpy import linalg as LA

import numpy as np

SEUIL = 100  # seuil gris entre 0, 255


def seuilgris(matrice, seuil):
    _, seuil_image = cv2.threshold(matrice, seuil, 255, cv2.THRESH_BINARY)
    return seuil_image


def gaussian(matrice, taille=(7, 7)):
    return cv2.GaussianBlur(matrice, taille, 0)


def sobel(matrice, ksize=3):
    sobelx64f = cv2.Sobel(matrice, cv2.CV_16S, 0, 1, ksize=ksize)
    sobely64f = cv2.Sobel(matrice, cv2.CV_16S, 1, 0, ksize=ksize)
    #abs_grad_x = cv2.convertScaleAbs(sobelx64f)
    #abs_grad_y = cv2.convertScaleAbs(sobely64f)
    #cv2.imshow('sobelx', abs_grad_x)
    #cv2.imshow('sobely', abs_grad_y)
    gradient = cv2.sqrt(cv2.pow(sobelx64f, 2) + cv2.pow(sobely64f, 2))
    #gradient = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
    return gradient


image = cv2.imread('images/6193.jpg')
gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

gaussian = gaussian(gray_image,(3,3))

median = cv2.medianBlur(gray_image, 81)

# toto = np.zeros((len(median), len(median[0])), np.int32)
median = cv2.subtract(gray_image,median)

#for x in range(len(median)):
#    for y in range(len(median[x])):
#        if median[x][y] < 0:
#            median[x][y] = 0
print(median)
# median = cv2.subtract(gray_image, median, dst, mask, dtype);

gradient = sobel(gaussian,3)

# gradient = cv2.convertScaleAbs(gradient)

# final = cv2.addWeighted( sobelx64f, 0.5, sobely64f, 0.5, 0)
# save = gray_image - median

# blur = cv2.blur(median,(5,5),0)

seuil_image = seuilgris(gradient, SEUIL)

# im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)


laplacian64 = cv2.Laplacian(seuil_image, cv2.CV_64F)

plt.subplot(5, 1, 1), plt.imshow(image, cmap='gray')
plt.title('Original'), plt.xticks([]), plt.yticks([])

plt.subplot(5, 1, 3), plt.imshow(seuil_image, cmap='gray')
plt.title('contour'), plt.xticks([]), plt.yticks([])

plt.subplot(5, 1, 5), plt.imshow(gradient, cmap='gray')
plt.title('Gradient'), plt.xticks([]), plt.yticks([])

plt.show()

# cv2.imshow('color_image',image)
cv2.imshow('median', median)
cv2.imshow('sobel', seuil_image)
cv2.waitKey(0)  # Waits forever for user to press any key
cv2.destroyAllWindows()  # Closes displayed windows
