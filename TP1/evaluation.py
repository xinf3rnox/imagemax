import sys, getopt
import cv2 as cv2


def evaluation(imageTrue, imagePerso):
    compteurTP = 0
    compteurFN = 0
    compteurFP = 0
    # Detection des TP/FP/FN
    for i in range(len(imageTrue)):
        for j in range(len(imageTrue[i])):
            pointTrue = imageTrue[i][j]
            pointPerso = imagePerso[i][j]
            # Les points sont forcement égaux à 255 ou 0
            if pointTrue == 255 and pointPerso == 255:
                # True Positive
                compteurTP += 1
            elif pointPerso == 255:
                # Verification de si c'est un TP ou un FP
                # On regarde dans les points alentours
                truePositive = False
                for k in range(9):
                    decallageLigne = (k % 3) - 1
                    decallageColonne = (k // 3) - 1
                    if (decallageLigne == -1 and i == 0) or (decallageLigne == 1 and i == len(imageTrue) - 1):
                        continue
                    if (decallageColonne == -1 and j == 0) or (decallageColonne == 1 and j == len(imageTrue[i]) - 1):
                        continue
                    pointTrueDecalle = imageTrue[(i + decallageLigne)][(j + decallageColonne)]
                    if pointTrueDecalle == 255:
                        # C'est un true positif
                        truePositive = True
                        break
                if truePositive:
                    compteurTP += 1
                else:
                    compteurFP += 1
            elif pointTrue == 255:
                # Verification de si c'est pas un TP ou un FN
                truePositive = False
                for k in range(9):
                    decallageLigne = (k % 3) - 1
                    decallageColonne = (k // 3) - 1
                    if (decallageLigne == -1 and i == 0) or (decallageLigne == 1 and i == len(imageTrue) - 1):
                        continue
                    if (decallageColonne == -1 and j == 0) or (decallageColonne == 1 and j == len(imageTrue[i]) - 1):
                        continue
                    pointPersoDecalle = imagePerso[(i + decallageLigne)][(j + decallageColonne)]
                    if pointPersoDecalle == 255:
                        # C'est un true positif
                        truePositive = True
                        break
                if truePositive:
                    compteurTP += 1
                else:
                    compteurFN += 1

    # Calcul de p et de r
    p = 0
    r = 0
    if compteurTP + compteurFP != 0:
        p = compteurTP / (compteurTP + compteurFP)
    if compteurTP + compteurFN != 0:
        r = compteurTP / (compteurTP + compteurFN)

    return p, r


def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('evaluation.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('evaluation.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    imageTrue = cv2.imread(inputfile, 0)
    imagePerso = cv2.imread(outputfile, 0)

    p, r = evaluation(imageTrue, imagePerso)
    # Affichage
    print(p)
    print(r)


if __name__ == "__main__":
    main(sys.argv[1:])
