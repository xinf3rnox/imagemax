import cv2 as cv2
from matplotlib import pyplot as plt
import sys, getopt
from seuilGris import *
from evaluation import *


def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('laplacian.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('laplacian.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    image = cv2.imread(inputfile)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    laplace = cv2.Laplacian(gray_image, -1)
    seuil_gris = seuilgris(laplace, 110, cv2.THRESH_BINARY)
    cv2.imwrite(outputfile, seuil_gris)
    # p,r = evaluation(seuil_gris,result)
    # print(i,p,r)


if __name__ == "__main__":
    main(sys.argv[1:])
