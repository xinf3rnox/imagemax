######################################
##									##				
##				README				##
##									##
######################################

Afin de lancer nos algorithmes, il faut dans un premier temps installer les requirements présent dans requirements.txt
Pour cela:
	- Installer python 3.7
	- Ouvrer une console dans le répertoire du projet
	- Taper la commande suivante : "pip install -r requirements.txt" et valider
	
Une fois cela effectué, pour lancer les différents programmes, veuillez utiliser les commandes suivantes:

	# Seuil de gris
	"seuilGris.py -i <inputfile> -o <outputfile>" avec inputfile et outputfile les chemins de l'image d'entrée et de l'image de sortie
	
	# Sobel
	"sobel.py -i <inputfile> -o <outputfile>" avec inputfile et outputfile les chemins de l'image d'entrée et de l'image de sortie
	
	# Laplacian
	"laplacian.py -i <inputfile> -o <outputfile>" avec inputfile et outputfile les chemins de l'image d'entrée et de l'image de sortie
	 
	# Evaluation
	"evaluation.py -i <inputfile> -o <outputfile>" avec inputfile = image de la fissure parfaite et outputfile = votre image
	
	# Algorithme du meilleur résultat
	"bestAlgo.py -i <inputfile> -o <outputfile>" avec inputfile = image de la fissure parfaite et outputfile = image initiale