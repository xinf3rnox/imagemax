import cv2 as cv2
from matplotlib import pyplot as plt
import sys, getopt


def seuilgris(matrice, seuil, type=cv2.THRESH_BINARY_INV):
    _, seuil_image = cv2.threshold(matrice, seuil, 255, type)
    return seuil_image


def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('seuilGris.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('seuilGris.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    image = cv2.imread(inputfile)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    after_seuil = seuilgris(gray_image, 20)  # Seuil fixe pour l'instant
    cv2.imwrite(outputfile, after_seuil)


if __name__ == "__main__":
    main(sys.argv[1:])
