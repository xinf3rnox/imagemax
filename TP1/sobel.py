import cv2 as cv2
from matplotlib import pyplot as plt
import sys, getopt
import seuilGris
import numpy as np
from math import *


def sobel(matrice, ksize=3):
    sobelx64f = cv2.Sobel(matrice, -1, 1, 0, ksize=ksize)
    sobely64f = cv2.Sobel(matrice, -1, 0, 1, ksize=ksize)
    # abs_grad_x = cv2.convertScaleAbs(sobelx64f)
    # abs_grad_y = cv2.convertScaleAbs(sobely64f)
    # cv2.imshow('sobelx', abs_grad_x)
    # cv2.imshow('sobely', abs_grad_y)
    # gradient = cv2.sqrt(cv2.add(cv2.pow(sobelx64f, 2),cv2.pow(sobely64f, 2)))

    gradient = np.zeros((len(sobelx64f), len(sobelx64f[0]), 1), dtype="uint8")
    for i in range(len(sobelx64f)):
        for x in range(len(sobelx64f[i])):
            gradient[i][x] = sqrt(pow(sobelx64f[i][x], 2) + pow(sobely64f[i][x], 2))
    # gradient = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
    return gradient


def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('sobel.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('sobel.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg
    image = cv2.imread(inputfile)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    after_sobel = sobel(gray_image)  # Seuil fixe pour l'instant
    seuil_gris = seuilGris.seuilgris(after_sobel, 198, cv2.THRESH_BINARY)
    cv2.imwrite(outputfile, seuil_gris)


if __name__ == "__main__":
    main(sys.argv[1:])
