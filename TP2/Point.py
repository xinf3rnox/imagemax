from math import pow


class Point:

    def __init__(self, x, y, value=0):
        self.x = x
        self.y = y
        self.value = value
        self.voisin = []
        self.similaire = None
        self.similaire_voisin = 1000000000000000000

    def equals(self, image1, image2, keypoint, rayon, skip, sumlimit):
        if abs(int(self.value) - int(keypoint.value)) > skip:
            return
        u1 = moyenne_patch(image1, self.x, self.y, rayon)
        u2 = moyenne_patch(image2, keypoint.x, keypoint.y, rayon)

        sum = 0
        for x in range(-rayon, rayon):
            for y in range(-rayon, rayon):
                sum += pow((image1[self.x + x][self.y + y] - u1) - (image2[keypoint.x + x][keypoint.y + y] - u2), 2)

        if sum < (rayon * 2 * rayon * 2) * pow(sumlimit, 2) and sum < self.similaire_voisin and sum < keypoint.similaire_voisin:
        #if sum < (rayon * 2 * rayon * 2) * pow(32, 2):
            if keypoint.similaire is not None:
                keypoint.reset()
            if self.similaire is not None:
                self.reset()
            self.similaire_voisin = sum
            self.similaire = keypoint
            keypoint.similaire = self
            keypoint.similaire_voisin = sum

    def reset(self):
        self.similaire = None
        self.similaire_voisin = 1000000000000000000

def moyenne_patch(image, x, y, rayon):
    minx = x - rayon
    miny = y - rayon
    maxx = x + rayon
    maxy = y + rayon

    res = 0
    for x in range(minx, maxx):
        for y in range(miny, maxy):
            res += image[x][y]
    return res / ((rayon*2)*(rayon*2))
