import time
import cv2 as cv2
import sys, getopt
import numpy as np
import fast as ft
import random
import os

SKIP = 40
POW = 20
TRESH = 100
RAYON = 3
DIFF_MAX = 1

from Point import Point


def main(argv):
    image1 = ''
    image2 = ''
    try:
        opts, args = getopt.getopt(argv, "hi1:i2:", ["image1=", "image2="])
    except getopt.GetoptError:
        print('bestAlgo.py -i1 <inputfile> -i2 <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('bestAlgo.py -i1 <inputfile> -i2 <outputfile>')
            sys.exit()
        elif opt in ("-i1", "--image1"):
            image1 = arg
        elif opt in ("-i2", "--image2"):
            image2 = arg

    directory = "tresh" + str(TRESH) + "-pow" + str(POW) + "-skip" + str(SKIP) + "-rayon" + str(RAYON)
    import os
    if not os.path.exists(directory):
        os.makedirs(directory)

    # read image 1
    image1 = cv2.imread(image1, 0)
    listKeyPoint1 = read_keypoint(image1)
    image2 = cv2.imread(image2, 0)
    listKeyPoint2 = read_keypoint(image2)

    listKeyPoint1 = check_same_point(listKeyPoint1, listKeyPoint2, image2, image1)

    print_image_keypoint(directory + "/image_all_kp.png", listKeyPoint1, image1, image2)

    nb_inliers = 0
    group_inliers = []
    keypoints_who_is_used_to_create_H = []
    for runner in range(0, 2000):
        compteur = 0
        keypoints = []
        while compteur < 4:
            rand_keypoint = random.choice(listKeyPoint1)
            if rand_keypoint.similaire is not None:
                keypoints.append(rand_keypoint)
                compteur += 1

        matrix_homographic = build_matrix_homographic(keypoints)

        np.set_printoptions(suppress=True)

        H = convert_homo_to_H33(matrix_homographic)
        # test si c'est ok
        is_erroned = False

        for keypoint in keypoints:
            diff_pos = test_diff_pos_between_image(keypoint, H)
            # il peut avoir des erreur d'arrondi
            if abs(diff_pos[0][0] - keypoint.similaire.x + diff_pos[1][0] - keypoint.similaire.y) > 1:
                # print(diff_pos)
                # print(keypoint.similaire.x)
                # print(keypoint.similaire.y)
                # print("ERROR : homographic in error")
                is_erroned = True
                break
        if is_erroned:
            pass
        # end test
        output_group_inliers = find_number_inliers(listKeyPoint1, H, DIFF_MAX)
        if nb_inliers < len(output_group_inliers):
            nb_inliers = len(output_group_inliers)
            group_inliers = output_group_inliers
            keypoints_who_is_used_to_create_H = keypoints

    print(len(group_inliers))

    print_image_keypoint(directory + "/image_kp_after-bestH.png", group_inliers, image1, image2)
    print_image_keypoint(directory + "/image_kp_create_H.png", keypoints_who_is_used_to_create_H, image1, image2)
    # todo : supp c'est pour tester
    H = convert_homo_to_H33(build_matrix_homographic((group_inliers)))

    point00 = test_diff_pos_between_image(Point(x=0, y=0), H)
    pointMAXY0 = test_diff_pos_between_image(Point(x=len(image1) - 1, y=0), H)
    point0MAXX = test_diff_pos_between_image(Point(x=0, y=len(image1[0]) - 1), H)
    pointMAXMAX = test_diff_pos_between_image(Point(x=len(image1) - 1, y=len(image1[0]) - 1), H)

    MINX = int(min(point00[0][0], pointMAXY0[0][0], point0MAXX[0][0], pointMAXMAX[0][0], 0))
    MAXX = int(max(point00[0][0], pointMAXY0[0][0], point0MAXX[0][0], pointMAXMAX[0][0], len(image2)))
    MINY = int(min(point00[1][0], pointMAXY0[1][0], point0MAXX[1][0], pointMAXMAX[1][0], 0))
    MAXY = int(max(point00[1][0], pointMAXY0[1][0], point0MAXX[1][0], pointMAXMAX[1][0], len(image2[0])))

    matrix = np.zeros((MAXX - MINX, MAXY - MINY), dtype=np.uint8)


    H1 = np.linalg.inv(H)


    for x in range(0, len(image2)):
        for y in range(0, len(image2[0])):
            matrix[x - MINX][y - MINY] = image2[x][y]

    for x in range(0, len(matrix)):
        for y in range(0, len(matrix[0])):
            new_pos = test_diff_pos_between_image(Point(x=x + MINX, y=y + MINY), H1)
            if new_pos[0][0] >= 0 and new_pos[0][0] < len(image1) - 1 and new_pos[1][0] >= 0 and new_pos[1][0] < len(
                    image1[0]) - 1:
                matrix[int(new_pos[0][0])][int(new_pos[1][0])] = image1[int(new_pos[0][0])][int(new_pos[1][0])]

    for x in range(0, len(matrix)):
        for y in range(0, len(matrix[0])):
            if matrix[x][y] == 0:
                fill_black_hole(matrix, x, y)
    cv2.imwrite(directory + "/image_fusion.png", matrix)


def fill_black_hole(matrix, x, y):
    val = 0
    nb = 0
    if x > 0:
        val += matrix[x - 1][y]
        nb += 1
    if x < len(matrix) - 2:
        val += matrix[x + 1][y]
        nb += 1

    if y > 0:
        val += matrix[x][y - 1]
        nb += 1
    if y < len(matrix[0]) - 2:
        val += matrix[x][y + 1]
        nb += 1
    if x > 0 and y > 0:
        val += matrix[x - 1][y - 1]
        nb += 1
    if x > 0 and y < len(matrix[0]) - 2:
        val += matrix[x - 1][y + 1]
        nb += 1
    if x < len(matrix) - 2 and y > 0:
        val += matrix[x + 1][y - 1]
        nb += 1
    if x < len(matrix) - 2 and y < len(matrix[0]) - 2:
        val += matrix[x + 1][y + 1]
        nb += 1
    return val / nb


def convert_homo_to_H33(homo):
    W, U, VT = cv2.SVDecomp(homo)

    V = np.matrix.transpose(VT)

    tmpH = V[:, 7]

    H = np.zeros((3, 3))
    for i in range(0, len(tmpH)):
        H[int(i / 3)][int(i % 3)] = tmpH[i]
    return H


def test_diff_pos_between_image(keypoint, H):
    ptImg1 = np.zeros((3, 1))
    ptImg1[0][0] = keypoint.x
    ptImg1[1][0] = keypoint.y
    ptImg1[2][0] = 1
    res = np.matmul(H, ptImg1)
    res[0][0] = res[0][0] / res[2][0]
    res[1][0] = res[1][0] / res[2][0]
    res[2][0] = res[2][0] / res[2][0]
    return res


def find_number_inliers(listKeyPoint1, H, max_range):
    output_group_inliers = []
    for keypoint in listKeyPoint1:
        if keypoint.similaire is not None:
            diff = test_diff_pos_between_image(keypoint, H)
            if abs((keypoint.similaire.x - int(diff[0][0])) + (keypoint.similaire.y - int(diff[1][0]))) < max_range:
                output_group_inliers.append(keypoint)
    return output_group_inliers


def print_image_keypoint(imagename, listKeyPoint, image1, image2):
    matrix = np.zeros((len(image2), len(image1[0]) + len(image2[0])), dtype=np.uint8)

    print(len(image1), len(image1[0]) + len(image2[0]))

    print(len(matrix), len(matrix[0]))
    for x in range(0, len(image1)):
        for y in range(0, len(image1[x])):
            matrix[x][y] = image1[x][y]

    for x in range(0, len(image2)):
        for y in range(len(image1[0]), len(image1[0]) + len(image2[0])):
            matrix[x][y] = image2[x][y - len(image1[0])]

    matrix = cv2.cvtColor(matrix, cv2.COLOR_GRAY2RGB)

    matrix = draw_voisin(listKeyPoint, matrix, len(image1[0]))

    cv2.imwrite(imagename, matrix)


def draw_voisin(listKeyPoint1, matrix, width):
    for keypoint in listKeyPoint1:
        cv2.circle(matrix, (keypoint.y, keypoint.x), 4, (0, 0, 255))
        if keypoint.similaire is not None:
            cv2.circle(matrix, (width + keypoint.similaire.y, keypoint.similaire.x), 4, (0, 0, 255))
            cv2.line(matrix, (keypoint.y, keypoint.x), (width + keypoint.similaire.y, keypoint.similaire.x),
                     (255, 0, 0), 1)
    return matrix


def check_same_point(listKeyPoint1, listKeyPoint2, image2, image1):
    print("debut equals")
    for keypoint in listKeyPoint1:
        for keypoint2 in listKeyPoint2:
            keypoint.equals(image1, image2, keypoint2, RAYON, SKIP, POW)
    print("fin equals")
    return listKeyPoint1


def build_matrix_homographic(keypoints):
    compteur = 0
    nb_line = 2 * len(keypoints)
    matrix = np.zeros((nb_line, 9))
    for keypoint in keypoints:
        matrix[compteur][0] = keypoint.x
        matrix[compteur][1] = keypoint.y
        matrix[compteur][2] = 1
        matrix[compteur][3] = 0
        matrix[compteur][4] = 0
        matrix[compteur][5] = 0
        matrix[compteur][6] = -(keypoint.similaire.x * keypoint.x)
        matrix[compteur][7] = -(keypoint.similaire.x * keypoint.y)
        matrix[compteur][8] = -(keypoint.similaire.x)
        matrix[compteur + 1][0] = 0
        matrix[compteur + 1][1] = 0
        matrix[compteur + 1][2] = 0
        matrix[compteur + 1][3] = keypoint.x
        matrix[compteur + 1][4] = keypoint.y
        matrix[compteur + 1][5] = 1
        matrix[compteur + 1][6] = -(keypoint.similaire.y * keypoint.x)
        matrix[compteur + 1][7] = -(keypoint.similaire.y * keypoint.y)
        matrix[compteur + 1][8] = -(keypoint.similaire.y)
        compteur += 2
    return matrix


def read_keypoint(img):
    kp = ft.fast(img, TRESH)
    # ft.showCorners(img, kp)

    print("Total Keypoints without nonmaxSuppression: ", len(kp))

    return kp


if __name__ == "__main__":
    start = time.clock()
    main(sys.argv[1:])
    elapsed = time.clock()
    elapsed = elapsed - start
    print("Time spent in (function name) is: ", elapsed)
