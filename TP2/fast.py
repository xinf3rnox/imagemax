import cv2 as cv2

from Point import Point


def fast(img, threshold=100, nonmaxSuppression=False):
    key_points = []
    for x in range(3, len(img) - 4):
        for y in range(3, len(img[x]) - 4):
            if check_is_keypoint(img, x, y, threshold):
                key_points.append(Point(x, y, img[x][y]))
    return key_points


def check_is_keypoint(img, x, y, treshold):
    list = get_all_point_in_circle(img, x, y)
    SUP = 0
    INF = 0
    EQUALS = 0

    for i in range(len(list) + 8):
        if list[i % 16] < img[x][y] - treshold:
            INF += 1
            SUP = 0
        elif list[i % 16] > img[x][y] + treshold:
            INF = 0
            SUP += 1
        else:
            EQUALS += 1
            if img[x][y] - treshold < list[(i + 8) % 16] < img[x][y] + treshold:
                return False
            INF = 0
            SUP = 0
        if SUP >= 9 or INF >= 9:
            return True

    return False


def get_all_point_in_circle(img, x, y):
    list = []

    list.append(img[x][y - 3])
    list.append(img[x + 1][y - 3])

    list.append(img[x + 2][y - 2])

    list.append(img[x + 3][y - 1])
    list.append(img[x + 3][y])
    list.append(img[x + 3][y + 1])

    list.append(img[x + 2][y + 2])

    list.append(img[x + 1][y + 3])
    list.append(img[x][y + 3])
    list.append(img[x - 1][y + 3])

    list.append(img[x - 2][y + 2])

    list.append(img[x - 3][y + 1])
    list.append(img[x - 3][y])
    list.append(img[x - 3][y - 1])

    list.append(img[x - 2][y - 2])

    list.append(img[x - 1][y - 3])

    return list


def showCorners(img, kp):
    for key_point in kp:
        cv2.circle(img, (key_point.y, key_point.x), 4, (0, 0, 255))
