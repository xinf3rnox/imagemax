#TP3 OBJET CIRCULAIRE

JEAN Kévin et YEN Arthur

#Prerequis

installer les paquets pythons :

```python
    pip install -r requirements.txt
```

#Execution

Lancer la detection de cercle basique :

```python
    main.py -i images/monimage.png
```

Pour l'exercice 3-1 :

```python
    fast_3_1.py -i images/monimage.png
```

Pour l'exercice 3-2 :

```python
    fast_3_2.py -i images/monimage.png
```


Ceci va créer un fichier show-circle.png a la racine du dossier.