import math


class Accumulateur:

    def __init__(self, sizex, sizey, sizerad):
        self.acc = [[[0 for k in range(sizerad)] for j in range(sizex)] for i in
                    range(sizey)]

    def increment_acc(self, x, y, radius):
        if x < len(self.acc) and y < len(self.acc[x]):
            self.acc[x][y][radius] += 1

    def normalize(self, minrad, deltarad):
        for x in range(len(self.acc)):
            for y in range(len(self.acc[x])):
                for r in range(len(self.acc[x][y])):
                    value = self.acc[x][y][r]
                    if value == 0:
                        pass
                    #print(self.acc[x][y][r],(math.pi * 2 * (r * deltarad + minrad)),self.acc[x][y][r] / (math.pi * 2 * (r * deltarad + minrad)))
                    self.acc[x][y][r] = self.acc[x][y][r] / (math.pi * 2 * (r * deltarad + minrad))

    # retounre une liste de liste contenant [x,y,radius,valeurVotant]
    def get_top_list(self, max_circle=10, patch=10):
        list = []
        for x in range(len(self.acc)):
            for y in range(len(self.acc[x])):
                for r in range(len(self.acc[x][y])):
                    value = self.acc[x][y][r]
                    if value == 0:
                        pass
                    if len(list) == max_circle:
                        index = -1
                        indexdelete = -1
                        for e in range(len(list)):
                            if list[e][0] - patch < x < list[e][0] + patch and list[e][1] - patch < y < list[e][
                                1] + patch and list[e][3] > value:
                                index = max_circle + 1
                                break
                            if list[e][0] - patch < x < list[e][0] + patch and list[e][1] - patch < y < list[e][
                                1] + patch and list[e][3] <= value:
                                indexdelete = e
                                break
                            if list[e][3] > value:
                                index = e
                        if -1 < index + 1 < max_circle:
                            list.pop(indexdelete)
                            list.insert(index + 1, [x, y, r, value])
                    else:
                        index = 0
                        for e in range(len(list)):
                            if list[e][3] > value:
                                index = e
                        list.insert(index + 1, [x, y, r, value])
        return list
