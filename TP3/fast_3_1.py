import math
import time
import cv2 as cv2
import sys, getopt
import numpy as np
import random

from acc import Accumulateur
from seuilGris import seuilgris
from sobel_direction import sobel

minradius = 5
maxradius = 30
deltaradius = 1
nombre_de_cercle = 5
# Eviter d'afficher un cercle qui sont similaire dans une meme zone
cube_patch = 10
delta_cone = 20


def main(argv):
    image1 = ''
    image2 = ''
    try:
        opts, args = getopt.getopt(argv, "hi:", ["image="])
    except getopt.GetoptError:
        print('bestAlgo.py -i <inputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('bestAlgo.py -i <inputfile>')
            sys.exit()
        elif opt in ("-i", "--image"):
            image1 = arg

    image_color = cv2.imread(image1)
    image = cv2.imread(image1, 0)

    # image_gaussian = cv2.GaussianBlur(image, (7, 7), 0, 0)

    seuil_gris, direction = sobel(image, 3)

    sobelx = cv2.Sobel(image, cv2.CV_32F, 1, 0, ksize=3)
    sobely = cv2.Sobel(image, cv2.CV_32F, 0, 1, ksize=3)
    phase = cv2.phase(sobelx, sobely, angleInDegrees=True)
    cv2.imwrite("gradient.png", seuil_gris)

    max = 0
    for i in range(1, len(seuil_gris) - 1):
        for x in range(1, len(seuil_gris[i]) - 1):
            if seuil_gris[i][x][0] > max:
                max = seuil_gris[i][x]

    t = max * 0.98
    for i in range(1, len(seuil_gris) - 1):
        for x in range(1, len(seuil_gris[i]) - 1):
            if seuil_gris[i][x] < t:
                seuil_gris[i][x] = 0

    image_direction = image_color.copy()
    for y in range(len(image) - 1, 0, -1):
        for x in range(len(image[0])):
            if seuil_gris[y][x] == 255:
                cv2.arrowedLine(image_direction, (x, y),
                                (int(x + 5 * -math.cos(direction[y][x])), int(y + 5 * -math.sin(direction[y][x]))),
                                (125, 0, 0))
    # seuil_gris = seuilgris(after_sobel, 50, cv2.THRESH_BINARY)

    acc = Accumulateur(len(image), len(image[0]), (maxradius - minradius) // deltaradius)

    cv2.imwrite("corner-detection.png", seuil_gris)
    cv2.imwrite("corner-detection-direction.png", image_direction)

    for y in range(len(image)):
        for x in range(len(image[0])):
            if seuil_gris[y][x] == 255:
                incrementacc(x, y, acc, int(direction[y][x]))

    acc.normalize(minradius, deltaradius)

    list_circle = acc.get_top_list(nombre_de_cercle, cube_patch)

    for point in list_circle:
        cv2.circle(image_color, (point[0], point[1]), (point[2] * deltaradius) + minradius, (255, 0, 0))

    print(list_circle)
    cv2.imwrite("show-circle.png", image_color)


# https://en.wikipedia.org/wiki/Midpoint_circle_algorithm

def incrementacc(x0, y0, acc, angle):
    for k in range((maxradius - minradius) // deltaradius):
        list_already_put = []
        radius = (k * deltaradius) + minradius
        for a in range(angle - delta_cone, angle + delta_cone):
            check_and_increment(int(x0 + radius * -math.cos(a)), int(y0 + radius * -math.sin(a)), k, list_already_put,
                                acc)


def check_and_increment(x, y, radius, list_already_put, acc):
    if (x, y) not in list_already_put:
        acc.increment_acc(x, y, radius)
        list_already_put.append((x, y))


def draw_point(x, y, image):
    if 0 < x < len(image) and 0 < y < len(image[x]):
        image[x][y] = (255, 0, 0)


if __name__ == "__main__":
    start = time.clock()
    main(sys.argv[1:])
    elapsed = time.clock()
    elapsed = elapsed - start
    print("Time spent in (function name) is: ", elapsed)
