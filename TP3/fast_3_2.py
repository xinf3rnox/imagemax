import time
import cv2 as cv2
import sys, getopt
import numpy as np
import random

from acc import Accumulateur
from seuilGris import seuilgris
from sobel import sobel

minradius = 10
maxradius = 40
deltaradius = 1
nombre_de_cercle = 2
# Eviter d'afficher un cercle qui sont similaire dans une meme zone
cube_patch = 5


def main(argv):
    image1 = ''
    try:
        opts, args = getopt.getopt(argv, "hi:", ["image="])
    except getopt.GetoptError:
        print('faster.py -i <inputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('faster.py -i <inputfile>')
            sys.exit()
        elif opt in ("-i", "--image"):
            image1 = arg

    taille_pyramide = 3
    image_color_init = cv2.imread(image1)
    image_init = cv2.imread(image1, 0)
    width_init = len(image_init)
    height_init = len(image_init[0])
    best_circle = []
    rad_up = maxradius//pow(2,taille_pyramide)
    rad_low = rad_up//2
    for runner in range(taille_pyramide,-1,-1):
        if runner != 0 :
            newWidth = width_init // pow(2,runner)
            newHeight = height_init // pow(2,runner)
            image = cv2.resize(image_init,(newWidth,newHeight))
            cv2.imwrite("resize.png", image)
        else :
            image = image_init

        seuil_gris = sobel(image, 3)

        cv2.imwrite("gradient.png", seuil_gris)

        max = 0
        for i in range(1, len(seuil_gris) - 1):
            for x in range(1, len(seuil_gris[i]) - 1):
                if seuil_gris[i][x] > max:
                    max = seuil_gris[i][x]

        t = max * 0.98
        for i in range(1, len(seuil_gris) - 1):
            for x in range(1, len(seuil_gris[i]) - 1):
                if seuil_gris[i][x] < t:
                    seuil_gris[i][x] = 0


        # gestion des radius
        if runner!=taille_pyramide:
            rad_up = 2*(rad_low+2)
            rad_low = rad_up//2
        if runner == 0:
            rad_low = minradius

        if rad_up<=rad_low :
            rad_up += abs(rad_low-rad_up) + rad_low//8
        acc = Accumulateur(len(image), len(image[0]), (rad_up - rad_low) // deltaradius)

        cv2.imwrite("corner-detection.png", seuil_gris)
        for y in range(len(image)):
            for x in range(len(image[0])):
                if seuil_gris[y][x] == 255:
                    incrementacc(x, y, acc,rad_low,rad_up)

        acc.normalize(rad_low, deltaradius)

        list_circle = acc.get_top_list(nombre_de_cercle, cube_patch)
        # for point in list_circle:
        #     point[2] = point[2]+(abs(rad_low-minradius))
        #TODO : Retester les anciens cercle si toujours good
        if len(best_circle) == 0:
            best_circle = list_circle
        else :
            # on remet les bons coordonées et rayon pour les points de la résolution précédente
            temp_list = []
            for point in best_circle:
                temp_list.append([point[0]*2, point[1]*2, point[2]*2, point[3]])
            temp_list = np.concatenate((list_circle,temp_list))
            # on recupere les N plus hautes valeurs
            best_circle = []
            for point in temp_list:
                if len(best_circle) != nombre_de_cercle:
                    if(len(best_circle)==0):
                        best_circle.append([point[0], point[1], point[2], point[3]])
                    else :
                        for i in range(len(best_circle)):
                            if abs(point[0]-best_circle[i][0]) > cube_patch or abs(point[1]-best_circle[i][1]) > cube_patch:
                                # on est dans pas dans le patch
                                if(point[3]>best_circle[i][3]):
                                    best_circle.insert(i + 1, [point[0], point[1], point[2], point[3]])
                                    break
                                else:
                                    best_circle.append([point[0], point[1], point[2], point[3]])
                                    break
                            else :
                                #on est dans le patch
                                if (point[3] > best_circle[i][3]):
                                    best_circle.insert(i + 1, [point[0], point[1], point[2], point[3]])
                                    best_circle.pop(i)
                                    break
                                else:
                                    break

                else :
                    for i in range(len(best_circle)):
                        if abs(point[0] - best_circle[i][0]) > cube_patch or abs(point[1] - best_circle[i][1]) > cube_patch:
                            #on est pas dans le patch
                            if (point[3] > best_circle[i][3]):
                                best_circle.insert(i + 1, [point[0], point[1], point[2], point[3]])
                                best_circle.pop()
                                break
                        else:
                            #on est dans le patch
                            if (point[3] > best_circle[i][3]):
                                best_circle.insert(i + 1, [point[0], point[1], point[2], point[3]])
                                best_circle.pop(i)
                                break
                            else :
                                break
    #Affichage des cercle
    for point in best_circle:
        cv2.circle(image_color_init, (int(point[0]), int(point[1])), (int(point[2]) * deltaradius) + minradius, (255, 0, 0))

    cv2.imwrite("show-circle.png", image_color_init)


# https://en.wikipedia.org/wiki/Midpoint_circle_algorithm

def incrementacc(x0, y0, acc,rad_low,rad_up):
    for k in range((rad_up - rad_low) // deltaradius):
        list_already_put = []
        radius = (k * deltaradius) + rad_low
        x = radius - 1
        y = 0
        dx = 1
        dy = 1
        err = dx - (radius << 1)

        while x >= y:
            check_and_increment(x0 + x, y0 + y, k, list_already_put, acc)
            check_and_increment(x0 + y, y0 + x, k, list_already_put, acc)
            check_and_increment(x0 - y, y0 + x, k, list_already_put, acc)
            check_and_increment(x0 - x, y0 + y, k, list_already_put, acc)
            check_and_increment(x0 - x, y0 - y, k, list_already_put, acc)
            check_and_increment(x0 - y, y0 - x, k, list_already_put, acc)
            check_and_increment(x0 + y, y0 - x, k, list_already_put, acc)
            check_and_increment(x0 + x, y0 - y, k, list_already_put, acc)

            # draw_point(x0 + x,y0 + y, image)
            # draw_point(x0 + y,y0 + x, image)
            # draw_point(x0 - y,y0 + x, image)
            # draw_point(x0 - x,y0 + y, image)
            # draw_point(x0 - x,y0 - y, image)
            # draw_point(x0 - y,y0 - x, image)
            # draw_point(x0 + y,y0 - x, image)
            # draw_point(x0 + x,y0 - y, image)

            if err <= 0:
                y += 1
                err += dy
                dy += 2

            if err > 0:
                x -= 1
                dx += 2
                err += dx - (radius << 1)


def check_and_increment(x, y, radius, list_already_put, acc):
    if (x, y) not in list_already_put:
        acc.increment_acc(x, y, radius)
        list_already_put.append((x, y))


def draw_point(x, y, image):
    if 0 < x < len(image) and 0 < y < len(image[x]):
        image[x][y] = (255, 0, 0)


if __name__ == "__main__":
    start = time.clock()
    main(sys.argv[1:])
    elapsed = time.clock()
    elapsed = elapsed - start
    print("Time spent in (function name) is: ", elapsed)
