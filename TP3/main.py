import time
import cv2 as cv2
import sys, getopt
import numpy as np
import random

from acc import Accumulateur
from seuilGris import seuilgris
from sobel import sobel

minradius = 5
maxradius = 30
deltaradius = 1
nombre_de_cercle = 4
# Eviter d'afficher un cercle qui sont similaire dans une meme zone
cube_patch = 3


def main(argv):
    image1 = ''
    image2 = ''
    try:
        opts, args = getopt.getopt(argv, "hi:", ["image="])
    except getopt.GetoptError:
        print('main.py -i <inputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('main.py -i <inputfile>')
            sys.exit()
        elif opt in ("-i", "--image"):
            image1 = arg

    image_color = cv2.imread(image1)
    image = cv2.imread(image1, 0)

    #image_gaussian = cv2.GaussianBlur(image, (7, 7), 0, 0)

    seuil_gris = sobel(image, 3)

    cv2.imwrite("gradient.png", seuil_gris)

    max = 0
    for i in range(1, len(seuil_gris) - 1):
        for x in range(1, len(seuil_gris[i]) - 1):
            if seuil_gris[i][x] > max:
                max = seuil_gris[i][x]

    t = max * 0.98
    for i in range(1, len(seuil_gris) - 1):
        for x in range(1, len(seuil_gris[i]) - 1):
            if seuil_gris[i][x] < t:
                seuil_gris[i][x] = 0

    # seuil_gris = seuilgris(after_sobel, 50, cv2.THRESH_BINARY)
    print((maxradius - minradius) // deltaradius)
    acc = Accumulateur(len(image), len(image[0]), (maxradius - minradius) // deltaradius)

    cv2.imwrite("corner-detection.png", seuil_gris)

    for y in range(len(image)):
        for x in range(len(image[0])):
            if seuil_gris[y][x] == 255:
                incrementacc(x, y, acc)

    acc.normalize(minradius, deltaradius)

    list_circle = acc.get_top_list(nombre_de_cercle, cube_patch)

    for point in list_circle:
        cv2.circle(image_color, (point[0], point[1]), (point[2] * deltaradius) + minradius, (255, 0, 0))

    print(list_circle)
    cv2.imwrite("show-circle.png", image_color)


# https://en.wikipedia.org/wiki/Midpoint_circle_algorithm

def incrementacc(x0, y0, acc):
    for k in range((maxradius - minradius) // deltaradius):
        list_already_put = []
        radius = (k * deltaradius) + minradius
        x = radius - 1
        y = 0
        dx = 1
        dy = 1
        err = dx - (radius << 1)

        while x >= y:
            check_and_increment(x0 + x, y0 + y, k, list_already_put, acc)
            check_and_increment(x0 + y, y0 + x, k, list_already_put, acc)
            check_and_increment(x0 - y, y0 + x, k, list_already_put, acc)
            check_and_increment(x0 - x, y0 + y, k, list_already_put, acc)
            check_and_increment(x0 - x, y0 - y, k, list_already_put, acc)
            check_and_increment(x0 - y, y0 - x, k, list_already_put, acc)
            check_and_increment(x0 + y, y0 - x, k, list_already_put, acc)
            check_and_increment(x0 + x, y0 - y, k, list_already_put, acc)

            # draw_point(x0 + x,y0 + y, image)
            # draw_point(x0 + y,y0 + x, image)
            # draw_point(x0 - y,y0 + x, image)
            # draw_point(x0 - x,y0 + y, image)
            # draw_point(x0 - x,y0 - y, image)
            # draw_point(x0 - y,y0 - x, image)
            # draw_point(x0 + y,y0 - x, image)
            # draw_point(x0 + x,y0 - y, image)

            if err <= 0:
                y += 1
                err += dy
                dy += 2

            if err > 0:
                x -= 1
                dx += 2
                err += dx - (radius << 1)


def check_and_increment(x, y, radius, list_already_put, acc):
    if (x, y) not in list_already_put:
        acc.increment_acc(x, y, radius)
        list_already_put.append((x, y))


def draw_point(x, y, image):
    if 0 < x < len(image) and 0 < y < len(image[x]):
        image[x][y] = (255, 0, 0)


if __name__ == "__main__":
    start = time.clock()
    main(sys.argv[1:])
    elapsed = time.clock()
    elapsed = elapsed - start
    print("Time spent in (function name) is: ", elapsed)
