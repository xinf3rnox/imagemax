import cv2 as cv2


def seuilgris(matrice, seuil, type=cv2.THRESH_BINARY_INV):
    _, seuil_image = cv2.threshold(matrice, seuil, 255, type)
    return seuil_image
