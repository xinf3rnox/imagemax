import cv2 as cv2
import numpy as np
from math import *


def sobel(matrice, ksize=3):
    gradient = np.zeros((len(matrice), len(matrice[0]), 1), dtype="uint8")

    # abs_grad_x = cv2.convertScaleAbs(sobelx64f)
    # abs_grad_y = cv2.convertScaleAbs(sobely64f)
    # cv2.imshow('sobelx', abs_grad_x)
    # cv2.imshow('sobely', abs_grad_y)
    # gradient = cv2.sqrt(cv2.add(cv2.pow(sobelx64f, 2),cv2.pow(sobely64f, 2)))
    for i in range(1, len(matrice) - 1):
        for x in range(1, len(matrice[i]) - 1):
            gradient[i][x] = 0

    for i in range(1, len(matrice) - 1):
        for x in range(1, len(matrice[i]) - 1):
            gx = cal_gradx(matrice, i, x)
            gy = cal_grady(matrice, i, x)
            res = sqrt((gx * gx) + (gy * gy))

            if res > 255:
                res = 255
            elif res < 0:
                res = 0

            gradient[i][x] = res
    # gradient = cv2.addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0)
    return gradient


def cal_gradx(matrice, row, col):
    gradx = matrice[row - 1][col - 1] * -1 - (2 * matrice[row][col - 1]) - matrice[row + 1][col - 1] + matrice[row - 1][
        col + 1] + (2 * matrice[row][col + 1]) + matrice[row + 1][col + 1]
    return gradx


def cal_grady(matrice, row, col):
    grady = matrice[row - 1][col - 1] * -1 - 2 * matrice[row - 1][col] - matrice[row - 1][col + 1] + matrice[row + 1][
        col - 1] + 2 * matrice[row + 1][col] + matrice[row + 1][col + 1]
    return grady
